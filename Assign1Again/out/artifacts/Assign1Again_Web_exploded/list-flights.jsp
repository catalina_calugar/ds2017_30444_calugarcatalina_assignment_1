<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <title>Flight List page</title>
</head>

<h1>Flight List page</h1>
<form action="FlightsServlet" method="get">
    <table style="text-align: center;" border="1px" cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th width="25px">idFlights</th>
            <th width="25px">flight number</th>
            <th width="25px">airplaneType</th>
            <th width="25px">departureCityId</th>
            <th width="25px">departureDateTime</th>
            <th width="25px">arrivalCityId</th>
            <th width="25px">arrivalDateTime</th>
            <th width="25px">Admin actions</th>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="flight" items="${flightList}">
            <tr>
                <td>${flight.idFlights}</td>
                <td>${flight.flightNumber}</td>
                <td>${flight.airplaneType}</td>
                <td>${flight.departureCityId}</td>
                <td>${flight.departureDateTime}</td>
                <td>${flight.arrivalCityId}</td>
                <td>${flight.arrivalDateTime}</td>

                <td>
                    <form action="FlightsServlet" method="get">
                        <input hidden type="text" name="method" value="EDIT">
                        <input hidden type="text" name="idFlights" value="${flight.idFlights}"/>
                        <input type="submit" value="Edit" />
                    </form>
                    <br/>

                    <form action="FlightsServlet" method="post">
                        <input hidden type="text" name="method" value="DELETE">
                        <input hidden type="text" name="idFlights" value="${flight.idFlights}"/>
                        <input type="submit" value="Delete"/>
                    </form>
                    <br/>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</form>
<button type="button" name="back" onclick="history.back()">back</button>
</body>
</html>