<%--
  Created by IntelliJ IDEA.
  User: Dan
  Date: 10/28/2017
  Time: 5:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create flight</title>
</head>
<body>

<form action="FlightsServlet" method="post">
    <input hidden type="text" name="method" value="CREATE">
    Enter flight number : <input type="text" name="flightNumber"> <BR>
    Enter airplane type : <input type="text" name="airplaneType"> <BR>
    Enter departure city : <input type="text" name="departureCity"> <BR>
    Enter departure date and time : <input type="text" name="departureDateTime"> <BR>
    Enter arrival city : <input type="text" name="arrivalCity"> <BR>
    Enter arrival date time : <input type="text" name="arrivalDateTime"> <BR>
    <input type="submit" />
</form>

</body>
</html>
