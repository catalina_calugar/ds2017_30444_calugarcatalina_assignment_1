<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cata_
  Date: 10/29/2017
  Time: 6:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Local time</title>
</head>
<body>
<h1>Cities List page</h1>
<form action="CitiesServlet" method="post">

    <p> Local hour for selected city is: ${localHour}  </p>
    <table style="text-align: center;" border="1px" cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th width="25px">idCities</th>
            <th width="25px">cityName</th>
            <th width="25px">longitude</th>
            <th width="25px">latitude</th>
            <th width="25px">Actions</th>

        </tr>
        </thead>
        <tbody>
        <c:forEach var="city" items="${cityList}">
            <tr>
                <td>${city.idCities}</td>
                <td>${city.cityName}</td>
                <td>${city.longitude}</td>
                <td>${city.latitude}</td>

                <td>
                    <form action="/CitiesServlet" method="post">
                        <input hidden type="text" name="idCities" value="${city.idCities}"/>
                        <input type="submit" value="Show local time"/>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</form>
<button type="button" name="back" onclick="history.back()">back</button>
</body>
</html>
