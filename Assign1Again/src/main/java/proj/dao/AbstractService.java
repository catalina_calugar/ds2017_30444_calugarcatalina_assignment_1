package proj.dao;

import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */
public interface AbstractService {
    public Object add(Object o);

    public Object delete(Object o);

    public Object edit(Object o);

    public Object getObject(int id);

    public  Object getObject(String s1, String s2);

    public  Object getObject(String s1);

    public List<Object> findAll();
}
