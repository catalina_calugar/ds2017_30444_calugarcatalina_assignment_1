package proj.dao;

import org.hibernate.*;
import proj.model.Users;

import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */

public class UserService implements AbstractService {
    private SessionFactory factory;

    public UserService(SessionFactory factory) {
        this.factory = factory;
    }

    @SuppressWarnings("unchecked")
    public Object add(Object o) {
        Users user = (Users) o;

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user == null || session == null ? null : user;
    }

    @SuppressWarnings("unchecked")
    public Object edit(Object o) {
        Users user = (Users) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user == null || session == null ? null : user;
    }

    @SuppressWarnings("unchecked")
    public Object delete(Object o) {
        Users user = (Users) o;

        if (getObject(user.getIdUsers()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(user);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return user == null ? null : user;
    }

    @SuppressWarnings("unchecked")
    public Object getObject(int id) {
        List<Users> users = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Users WHERE idUsers = :id");
            query.setParameter("id", id);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return users == null || session == null ? null : users.get(0);
    }

    @SuppressWarnings("unchecked")
    public Object getObject(String username, String password) {
        List<Users> users = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Users WHERE username = :username AND password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return users == null || session == null ? null : users.get(0);
    }

    @Override
    public Object getObject(String s1) {
        return null;
    }

    @Override
    public List<Object> findAll() {
        return null;
    }
}
