package proj.dao;

import org.hibernate.*;
import proj.model.Flights;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */
public class FlightsService implements AbstractService {

    private SessionFactory factory;

    public FlightsService(SessionFactory factory) {
        this.factory = factory;
    }

    public Object add(Object o) {
        Flights flight = (Flights) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flight == null || session == null ? null : flight;
    }

    public Object edit(Object o) {
        Flights flight = (Flights) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return flight == null || session == null ? null : flight;
    }

    public Object delete(Object o) {
        Flights flight = (Flights) o;

        if (getObject(flight.getIdFlights()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(flight);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return flight == null ? null : flight;
    }



    public Object getObject(int idflights) {
        List<Flights> flights = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flights WHERE idFlights = :idflights");
            query.setParameter("idflights", idflights);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return flights == null || session == null ? null : flights.get(0);
    }

    @Override
    public Object getObject(String s1, String s2) {
        return null;
    }

    @Override
    public Object getObject(String s1) {
        return null;
    }

    public List<Object> findAll() {
        List<Object> flights = new ArrayList<Object>();
        Session session = factory.openSession();
        Transaction tx = null;
        if (session != null) {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flights");
            flights = query.list();
        }
        return flights == null || session == null ? null : flights;
    }
}
