package proj.dao;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.hibernate.*;
import proj.model.Cities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */
public class CitiesService implements AbstractService {
    private SessionFactory factory;

    public CitiesService(SessionFactory factory) {
        this.factory = factory;
    }

    public Object add(Object o) {
        Cities city = (Cities) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return city == null || session == null ? null : city;
    }

    public Object edit(Object o) {
        Cities city = (Cities) o;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(city);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return city == null || session == null ? null : city;
    }

    public Object delete(Object o) {
        Cities city = (Cities) o;

        if (getObject(city.getIdCities()) != null) {
            Session session = factory.openSession();
            Transaction tx = null;
            try {
                tx = session.beginTransaction();
                session.delete(city);
                tx.commit();
            } catch (HibernateException e) {
                if (tx != null) {
                    tx.rollback();
                }
            } finally {
                session.close();
            }
        }
        return city == null ? null : city;
    }

    public Object getObject(int idCities) {
        List<Cities> citys = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Cities WHERE idCities = :idCities");
            query.setParameter("idCities", idCities);
            citys = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return citys == null || session == null ? null : citys.get(0);
    }

    public Object getObject(String cityName) {
        List<Cities> citys = null;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Cities WHERE cityName = :cityName");
            query.setParameter("cityName", cityName);
            citys = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return citys == null || session == null ? null : citys.get(0);
    }

    @Override
    public Object getObject(String s1, String s2) {
        return null;
    }

    public List<Object> findAll() {
        List<Object> cities = new ArrayList<Object>();

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Cities");
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return cities == null || session == null ? null : cities;
    }

    public Timestamp getCoordinates(float longitude, float latitude) {
        try {
            Timestamp givenDate = new Timestamp(System.currentTimeMillis());

            Integer rawOffset = 0;
            long localTime = givenDate.getTimezoneOffset() * 60 + givenDate.getTime() / 1000;
            String url = "https://maps.googleapis.com/maps/api/timezone/json";
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url)
                    .header("accept", "application/json")
                    .queryString("location", longitude + "," + latitude)
                    .queryString("timestamp", localTime).asJson();

            if (jsonResponse.getBody().getObject().get("rawOffset") != null) {
                rawOffset = (Integer) jsonResponse.getBody().getObject().get("rawOffset");
            }

            Integer dstOffset = (Integer) jsonResponse.getBody().getObject().get("dstOffset");
            Integer offset = dstOffset * 1000 + rawOffset * 1000;

            Timestamp output = new Timestamp(localTime * 1000 + offset);
            System.out.println(output);
            return output;
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }
}
