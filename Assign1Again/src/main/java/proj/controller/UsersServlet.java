package proj.controller;

import org.hibernate.cfg.Configuration;
import proj.model.Users;
import proj.dao.AbstractService;
import proj.dao.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cata_ on 10/28/2017.
 */

public class UsersServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private AbstractService service;

    public UsersServlet() {
        super();
        service = new UserService(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        RequestDispatcher rd;

        Object o = service.add(new Users(name, username, password, email));
        if (o != null) {
            rd = request.getRequestDispatcher("/index.jsp");
            request.setAttribute("user", (Users) o);
        } else {
            rd = request.getRequestDispatcher("/error.jsp");
        }
        rd.forward(request, response);

    }
}
