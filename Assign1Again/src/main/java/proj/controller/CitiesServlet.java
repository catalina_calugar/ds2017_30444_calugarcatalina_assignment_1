package proj.controller;

import org.hibernate.cfg.Configuration;
import proj.dao.AbstractService;
import proj.dao.CitiesService;
import proj.dao.UserService;
import proj.model.Cities;
import proj.model.Flights;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */
public class CitiesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private CitiesService service;

    public CitiesServlet() {
        super();
        service = new CitiesService(new Configuration().configure().buildSessionFactory());
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = null;

        List<Object> cityList = service.findAll();
        rd = request.getRequestDispatcher("/local-time.jsp");
        request.setAttribute("cityList", cityList);
        request.setAttribute("localHour", "");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = null;
        String idCities = request.getParameter("idCities");
        Cities c = (Cities) service.getObject(Integer.parseInt(idCities));
        float latitude = (float) c.getLatitude();
        float longitude = (float) c.getLongitude();

        Timestamp hour = service.getCoordinates(longitude, latitude);
        if (hour != null) {
            rd = request.getRequestDispatcher("/local-time.jsp");
            List<Object> cityList = service.findAll();
            request.setAttribute("cityList", cityList);
            request.setAttribute("localHour", hour);
            rd.forward(request, response);
        }
    }
}
