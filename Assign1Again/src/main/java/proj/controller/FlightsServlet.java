package proj.controller;

import org.hibernate.cfg.Configuration;
import proj.dao.AbstractService;
import proj.dao.CitiesService;
import proj.dao.FlightsService;
import proj.dao.UserService;
import proj.model.Cities;
import proj.model.Flights;
import proj.model.Users;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cata_ on 10/28/2017.
 */
public class FlightsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private AbstractService service;
    private AbstractService cityService;

    public FlightsServlet() {
        super();
        service = new FlightsService(new Configuration().configure().buildSessionFactory());
        cityService = new CitiesService(new Configuration().configure().buildSessionFactory());
    }


    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = null;
        HttpSession session = request.getSession();
        String method = request.getParameter("method");
        if (method != null && !method.isEmpty()) {
            if (((Users) session.getAttribute("loggedUser")).getUsername().equals("admin")) {
                Flights flight = (Flights) service.getObject(Integer.parseInt(request.getParameter("idFlights")));
                rd = request.getRequestDispatcher("/edit-flights.jsp");
                request.setAttribute("flight", flight);
            }else{
                rd = request.getRequestDispatcher("/user-page.jsp");
            }
        } else {
            List<Object> flightList = service.findAll();
            rd = request.getRequestDispatcher("/list-flights.jsp");
            request.setAttribute("flightList", flightList);
        }
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        String method = request.getParameter("method");
        String flightNumber = request.getParameter("flightNumber");
        String airplaneType = request.getParameter("airplaneType");
        String departureCity = request.getParameter("departureCity");
        String departureDateTime = request.getParameter("departureDateTime");
        String arrivalCity = request.getParameter("arrivalCity");
        String arrivalDateTime = request.getParameter("arrivalDateTime");
        RequestDispatcher rd;
        Object o = null;
        Timestamp at = null;
        Timestamp dt = null;
        if (departureDateTime != null) {
            dt = Timestamp.valueOf(departureDateTime);
        }
        if (arrivalDateTime != null) {
            at = Timestamp.valueOf(arrivalDateTime);
        }
        HttpSession session = request.getSession();

        if (((Users) session.getAttribute("loggedUser")).getUsername().equals("admin")) {
            if (method.equals("CREATE")) {
                Cities c1 = (Cities) cityService.getObject(departureCity);
                Cities c2 = (Cities) cityService.getObject(arrivalCity);
                o = service.add(new Flights(flightNumber, airplaneType, c1.getIdCities(), dt, c2.getIdCities(), at));
            } else if (method.equals("DELETE")) {
                String id = request.getParameter("idFlights");
                Flights f = (Flights) service.getObject(Integer.parseInt(id));
                o = service.delete(f);
            } else if (method.equals("EDIT")) {
                String id = request.getParameter("idFlights");
                Flights f = (Flights) service.getObject(Integer.parseInt(id));
                f.setAirplaneType(airplaneType);
                f.setArrivalCityId(Integer.parseInt(arrivalCity));
                f.setArrivalDateTime(at);
                f.setDepartureCityId(Integer.parseInt(departureCity));
                f.setDepartureDateTime(dt);
                f.setFlightNumber(flightNumber);
                o = service.edit(f);
            }
        }
        if (o != null) {
            rd = request.getRequestDispatcher("/admin-page.jsp");
            request.setAttribute("flights", (Flights) o);
        } else {
            rd = request.getRequestDispatcher("/user-page.jsp");
        }
        rd.forward(request, response);
    }

}
