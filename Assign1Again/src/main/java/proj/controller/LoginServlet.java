package proj.controller;

import org.hibernate.cfg.Configuration;
import proj.dao.AbstractService;
import proj.dao.UserService;
import proj.model.Users;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by cata_ on 10/31/2017.
 */
public class LoginServlet extends HttpServlet {
    private AbstractService service;

    public LoginServlet() {
        super();
        service = new UserService(new Configuration().configure().buildSessionFactory());
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = null;
        HttpSession session = request.getSession();
        String method = request.getParameter("method");

        if(method!= null && !method.isEmpty()){
            session.setAttribute("loggedUser", "dummy");
            rd = request.getRequestDispatcher("/index.jsp");
            request.setAttribute("method", "");
        }else {
            if(session != null || !session.getAttribute("loggedUser").equals("dummy")) {
                if (((Users) session.getAttribute("loggedUser")).getUsername().equals("admin")) {
                    rd = request.getRequestDispatcher("/admin-page.jsp");
                } else if (((Users) session.getAttribute("loggedUser")).getUsername().equals("user")) {
                    session.setAttribute("loggedUser", null);
                    rd = request.getRequestDispatcher("/user-page.jsp");
                }
            }else {
                session.setAttribute("loggedUser", "dummy");
                rd = request.getRequestDispatcher("/index.jsp");
            }
        }
        rd.forward(request, response);

    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        RequestDispatcher rd = null;

        String result = authenticate(username, password, request.getSession());

        if (result.equals("admin")) {
            rd = request.getRequestDispatcher("/admin-page.jsp");
            Users user = (Users) service.getObject(username, password);
            request.setAttribute("user", user);
        } else if (result.equals("user")) {
            rd = request.getRequestDispatcher("/user-page.jsp");
            Users user = (Users) service.getObject(username, password);
            request.setAttribute("user", user);
        } else {
            rd = request.getRequestDispatcher("/error.jsp");
        }
        rd.forward(request, response);
    }

    public String authenticate(String username, String password, HttpSession session) {
        Users user = null;
        user = (Users) service.getObject(username, password);
        if (user != null) {
            session.setAttribute("loggedUser", user);
            if ("admin".equalsIgnoreCase(username)) {
                return "admin";
            } else {
                return "user";
            }
        } else {
            return "failure";
        }
    }

}
