package proj.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by cata_ on 10/28/2017.
 */
@Entity
@Table(name = "flights", catalog = "testdb")
public class Flights implements java.io.Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idflights", unique = true, nullable = false)
    private int idFlights;
    @Column(name = "flightNumber", unique = true, nullable = false)
    private String flightNumber;
    @Column(name = "airplaneType", unique = true, nullable = false)
    private String airplaneType;
    @Column(name = "departureCityId", unique = true, nullable = false)
    private int departureCityId;
    @Column(name = "depatrureDateTime", unique = true, nullable = false)
    private Timestamp departureDateTime;
    @Column(name = "arrivalCityId", unique = true, nullable = false)
    private int arrivalCityId;
    @Column(name = "arrivalDateTime", unique = true, nullable = false)
    private Timestamp arrivalDateTime;

    public Flights() {
    }

    public Flights(String flightNumber, String airplaneType, int departureCityId, Timestamp departureDateTime, int arrivalCityId, Timestamp arrivalDateTime) {
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.departureDateTime = departureDateTime;
        this.arrivalCityId = arrivalCityId;
        this.arrivalDateTime = arrivalDateTime;
    }

    public Flights(int idFlights, String flightNumber, String airplaneType, int departureCityId, Timestamp departureDateTime, int arrivalCityId, Timestamp arrivalDateTime) {
        this.idFlights = idFlights;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.departureDateTime = departureDateTime;
        this.arrivalCityId = arrivalCityId;
        this.arrivalDateTime = arrivalDateTime;
    }

     public int getIdFlights() {
        return idFlights;
    }

    public void setIdFlights(int idFlights) {
        this.idFlights = idFlights;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public int getDepartureCityId() {
        return departureCityId;
    }

    public void setDepartureCityId(int departureCityId) {
        this.departureCityId = departureCityId;
    }

    public Timestamp getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Timestamp departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public int getArrivalCityId() {
        return arrivalCityId;
    }

    public void setArrivalCityId(int arrivalCityId) {
        this.arrivalCityId = arrivalCityId;
    }

    public Timestamp getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Timestamp arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }
}
