package proj.model;

import javax.persistence.*;

/**
 * Created by cata_ on 10/28/2017.
 */
@Entity
@Table(name = "cities", catalog = "testdb")
public class Cities implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcities", unique = true, nullable = false)
    private int idCities;
    @Column(name = "cityName", unique = true, nullable = false, length = 45)
    private String cityName;
    @Column(name = "longitude", unique = true, nullable = false)
    private Float longitude;
    @Column(name = "latitude", unique = true, nullable = false)
    private float latitude;

    public Cities() {
    }

    public Cities(String cityName, Float longitude, float latitude) {
        this.cityName = cityName;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Cities(int idCities, String cityName, Float longitude, float latitude) {
        this.idCities = idCities;
        this.cityName = cityName;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public int getIdCities() {
        return idCities;
    }

    public void setIdCities(int idCities) {
        this.idCities = idCities;
    }


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
