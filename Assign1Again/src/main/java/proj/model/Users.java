package proj.model;

import javax.persistence.*;

/**
 * Created by cata_ on 10/28/2017.
 */
@Entity
@Table(name = "users", catalog = "testdb")
public class Users implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idusers", unique = true, nullable = false)
    private int idUsers;
    @Column(name = "name", unique = true, nullable = false, length = 45)
    private String name;
    @Column(name = "username", unique = true, nullable = false, length = 45)
    private String username;
    @Column(name = "password", unique = true, nullable = false, length = 45)
    private String password;
    @Column(name = "email", unique = true, nullable = false, length = 45)
    private String email;

    public Users(){

    }

    public Users(String name, String username, String password, String email) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Users(int idUsers, String name, String username, String password, String email) {
        this.idUsers = idUsers;
        this.name = name;
        this.username = username;
        this.password = password;
        this.email = email;
    }


    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
